# A simple script to concatenate airport files. Could have used bash,
# but this will work cross-platform.
#
# A directory is provided that contains a list of apt.dat files. Only
# files ending in ".dat" are processed.
#
# Usage: julia process_oz_airports.jl <directory> > <output file>
#
# A properly-formatted apt.dat is printed to standard output
#
# Genapts850 fails on lighting types 107 and 108. We drop any lines
# containing these.

const header = """
I
1000 Australian airports extracted from X-Plane gateway, gateway.x-plane.com. Copyright <A9> 2013-2020, Robin A. Peel (robin@x-plane.com).   This data is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.  You should have received a copy of the GNU General Public License along with this program ("AptNavGNULicence.txt"); if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.

"""

concat_dat_files(directory,preprocess) = begin
    write(stdout,header)
    for one_file in readdir(directory)
        if one_file[(end-2):end] != "dat" continue end
        write(stdout,open(f->preprocess(read(f,String)),joinpath(directory,one_file)))
        write(stdout,"\n")
    end
    write(stdout,"99\n")
end

# Remove top 5 and last line
apt_preprocess(text_block) = begin
    all_lines = split(text_block,"\n")
    write(stderr,"$(length(all_lines)) for $(all_lines[7])\n")
    for (line_no,one_line) in enumerate(all_lines)
        if line_no < 6 continue end     # Header
        fields = split(one_line)
        if length(fields) == 0 continue end
        typeno = parse(Int,fields[1])
        if typeno >= 111 && typeno < 120  #linear feature
            if fields[end] == "108" || fields[end] == "107"
                write(stderr,"Dropping 108/107 lighting from $one_line\n")
                all_lines[line_no] = join(fields[1:(end-1)]," ")
                write(stderr,"Line now $(all_lines[line_no])\n")
            end
        end
    end
    join(all_lines[6:(end-2)],"\n")
end

if abspath(PROGRAM_FILE) == @__FILE__
    directory = ARGS[1]
    concat_dat_files(directory,apt_preprocess)
end
