#== 

A Julia script to make scenery
==============================

This script takes raw Australian landcover data, currently from OSM,
ALUM and NVIS and generates complete scenery tiles.

It has been tested under Linux and NSW,QLD landuse data, but all software
used is cross-platform so no changes should be necessary for other
platforms. Note however that some of the routines in SceneryTools.jl
create bash scripts - not sure if they would work in Windows.

For installation and usage instructions, please see the
end of the file.

Configuration
=============

The working directory layout is <scenery_top>/<project_top>/{work,data,rawdata},
which is similar to TerraGUI.

Pre-downloaded and unzipped Viewfinder Panorama height data should be
present in rawdata/VP-SRTM-3.
You must have an apt.dat file (distributed with FlightGear) available as well.  
Give the locations of these files in the .ini config file provided on
the command line.

==#

using ConfParser   #to read the configuration file
using Dates

# All of the data file handling routines

include(joinpath(@__DIR__,"SceneryTools.jl"))
include(joinpath(@__DIR__,"dodgy_point_fixer.jl"))

# Use the configuration to calculate some more directories and
# executable locations

setup(conf) = begin

    root_dir = conf["default"]["root"]
    scenery_top  = conf["default"]["scenery_top"]
    project_top = conf["default"]["project_top"]
    global elevations = joinpath(root_dir,"data","VP-SRTM-3") # Storage of height data
    global workdir = joinpath(root_dir,"work")
    mkpath(workdir)
    global scratchdir = joinpath(root_dir,"rawdata/scratch")
    mkpath(scratchdir)
    global datadir = joinpath(root_dir,"data")

    # Terragear executables
    installexedir = conf["default"]["installexedir"]
    global tgconstruct = joinpath(installexedir,"tg-construct")
    global ogr_decode = joinpath(installexedir,"ogr-decode")
    global cliff_decode = joinpath(installexedir,"cliff-decode")
    global rectify = joinpath(installexedir,"rectify_height")
    global chopper = joinpath(installexedir,"gdalchop")
    global terrafit = joinpath(installexedir,"terrafit")
    global genapts = joinpath(installexedir,"genapts850")


end

setup_tile(bottom,left,conf) = begin
    basename = "$(Int(abs(bottom)))S$(Int(abs(left)))E"   #letters wrong for northern hem
    root_dir = conf["default"]["root"]
    basedir = joinpath(root_dir,basename)
    datadir = joinpath(basedir,"data")
    scratchdir = joinpath(basedir,"rawdata/scratch")
    if !isdir(basedir)
        mkdir(basedir)
    end
    if !isdir(datadir)
        mkdir(datadir)
    end
    if !isdir(scratchdir)
        mkpath(scratchdir)
    end
    return basedir,datadir,scratchdir
end



#== The landcover table
Entries: 
    Geometry: 0 (Area), otherwise linewidth
    Smooth: true/false if areas need to have smoothed borders,
    Table of sources as Lists of LandCover subtypes
==#
get_landcover_recipes(;restrict="all") = begin
    # choose only those in the actual list
    all_landcovers = Dict(
    "Lake" =>  LandConfig(0,false,
            [OSM("waterway=riverbank",true,false,"river"),
             OSM("waterway=riverbank",false,true,"river2"),
             OSM( "water=river",true,false,"river3"),
             OSM( "water=river",false,true,"river4"),
             ALUM([610,620],"lake1"),
             OSM( "natural=water --tf reject-ways water=harbour,bay --tf reject-ways intermittent=yes",false,true,"lake2"),
             OSM( "natural=water --tf reject-relations water=harbour,bay --tf reject-relations intermittent=yes",true,false,"lake3"),
             OSM( "water=pond,canal",false,true,"pond"),
             OSM( "leisure=swimming_pool --tf accept-ways covered=no",false,false,"pool"),
             ]),
    "IntermittentLake" => LandConfig(0, false,
                           [OSM("natural=water --tf accept-ways intermittent=yes",false,true,"ilake1"),
                            OSM("natural=water --tf accept-relations intermittent=yes",true,false,"ilake2"),
                            OSM("wetland=lake --tf accept-ways intermittent=yes",false,true,"ilake3")]),
    "Estuary" => LandConfig(0, false,
              [
               OSM("water=harbour",true,false,"harbour2"),
               OSM("natural=bay",true,false,"bay1"),
                  OSM("water=bay",true,false,"bay2"),
               ]),
    "Littoral" => LandConfig(0, true,
            [OSM("natural=beach",true,false,"beach1"),
            OSM("natural=beach",false,false,"beach2")
             ]),
    "EvergreenBroadCover" => LandConfig(0, true,[
                                NVIS("(mvg_number >= 1 and mvg_number <= 10) or (mvg_number = 25 and dominant_stratum = 'U')","v-forest"),
    ]),
    "SavannaCover" => LandConfig(0, true, [
        NVIS("mvg_number >= 11 and mvg_number <= 13","v-savanna"),
    ]),
    "Scrub" => LandConfig(0, true, [
                NVIS("(mvg_number >= 15 and mvg_number <= 17) or (mvg_number = 25 and dominant_stratum = 'M')","v-scrub"),]),
    "Heath" => LandConfig(0, true, [
                NVIS("mvg_number = 18","v-heath"),]),
    "Stream" => LandConfig(5, false,
                 [OSM("waterway=river,stream,canal --tf reject-ways intermittent=yes",false,false,"stream")]),
    "Marsh" => LandConfig(0, true,
                [ALUM([650],"marsh")]),
    "Grassland" => LandConfig(0, true,
                    [NVIS("mvg_number >=19 and mvg_number <=21 ","v-grassland")]),
    "Rock" => LandConfig(0, true,
                [OSM("natural=bare_rock,scree",false,false,"o-rock")]),
    "Mangrove" => LandConfig(0, true,
                   [NVIS("mvg_number = 23 ","v-mangroves")]),
    "Industrial" => LandConfig(0, false,
                     [OSM("landuse=industrial --tf reject-relations industrial=port",true,false,"industrial1"),
                      OSM("landuse=industrial --tf reject-ways industrial=port",false,true,"industrial2"),
                      ALUM([550,551,555],"commercial"),
]),
    "Port"=> LandConfig(0, false,
              [OSM("industrial=port",false,true,"port1"),]),
    "Dump"=> LandConfig(0, false,
              [OSM("landuse=landfill",false,true,"dump1"),]),
    "Dirt"=> LandConfig(0, true,
              [NVIS("mvg_number = 27","v-dirt"),]),
    "OpenMining"=> LandConfig(0, false,
                    [ALUM([580],"mining")]),
        "Asphalt"=> LandConfig(0, false, [OSM("amenity=parking",false,true,"parking"),
                       OSM("leisure=pitch --tf accept-ways surface=paved",false,false,"paved_pitch")]),
    "Cemetery" => LandConfig(0, false,
               [OSM("landuse=cemetery",true,false,"cemetery1"),
                OSM("landuse=cemetery",false,true,"cemetery2")]),
    "Greenspace" => LandConfig(0, true,
                     [OSM("leisure=park",false,true,"park1"),
                      OSM("amenity=school",false,true,"park2"),
                      # Note we exclude public services as they include schools.
                        # ALUM and OSM do not always overlap perfectly leading to
                        # unsightly slivers of the lower-priority landcover, so we
                        # avoid doubling up.

                      ALUM([552,553],"public")]),
    "GrassCover" => LandConfig(0, false,
                [OSM("leisure=pitch --tf reject-ways surface=paved",false,true,"pitch1")]), #miss artificial surfaces
    "GolfCourse" => LandConfig(0, false,
           [OSM("leisure=golf_course",true,false,"golf1"),
            OSM("leisure=golf_course",false,true,"golf2")]),
    "DryCrop" => LandConfig(0, false,
              [ALUM([320,330],"drycrop")]),
    "AgroForest" => LandConfig(0, false,
                     [ALUM([310,410],"forestry")]),
    "IrrCrop" => LandConfig(0, false,
                    [ALUM([420,430,440,450,460],"irrigation")]),
    "Road-Motorway" => LandConfig(10, false,
                    [OSM("highway=motorway --tf reject-ways tunnel=* --tf reject-ways bridge=* ",false,false,"Road-Motorway")]),
    "Road-Trunk" => LandConfig(10, false,
                 [OSM("highway=trunk --tf reject-ways tunnel=* --tf reject-ways bridge=* ",false,false,"Road-Trunk")]),
    "Road-Primary" =>LandConfig(10, false,
                      [OSM("highway=primary --tf reject-ways tunnel=* --tf reject-ways bridge=* ",false,false,"Road-Primary")]),
    "Road-Secondary" => LandConfig(8, false,
                         [OSM("highway=secondary --tf reject-ways tunnel=* --tf reject-ways bridge=* ",false,false,"Road-Secondary")]),
"Road-Tertiary" => LandConfig(5, false,
                    [OSM("highway=tertiary --tf reject-ways surface=unpaved,compacted,fine_gravel,gravel,dirt,earth,ground --tf reject-ways bridge=*",
                      false,false,"Road-Tertiary")]),
    # Use unclassified roads for unpaved roads
"Road-Unclassified" => LandConfig(5, false,
                        [OSM("highway=tertiary,unclassified,residential,tertiary_link,road --tf reject-ways surface=paved,asphalt,concrete --tf reject-ways bridge=* ",false,false,"Road-Unclassified")]),
    "Railroad" => LandConfig(10, false,
                  [OSM("railway=rail --tf reject-ways tunnel=* --tf reject-ways bridge=* ",false,false,"Railroad")]),
    "SubUrban" => LandConfig(0, false,
               [ALUM([541],"suburban",0.00004)]),  #use a 4m buffer to drop small streets
    "Town" => LandConfig(0, false,
               [ALUM([542,543,544,545,540],"town",0.00004)])  #use a 4m buffer to drop small streets
    )
    if restrict != "all"
        return filter(x -> x.first in restrict, all_landcovers)
    else
        return all_landcovers
    end
end


make_coastline(redo,conf) = begin
    out_dir = joinpath(workdir,"Default")
    if !redo && isdir(joinpath(workdir,"Default"))
        println("No coastline added as $(joinpath(workdir,"Default")) exists")
        return
    else
        read(`rm -f $out_dir`)
    end
    project_top = conf["default"]["project_top"]
    intermediate = joinpath(scratchdir,"filter_coast.osm.pbf")
    final = joinpath(scratchdir,project_top*"_coastline.db")
    finalshape = joinpath(scratchdir,"Default.gpkg")
    if !redo && (isfile(finalshape) || isdir(finalshape))
        println("$finalshape exists, so skipping coastline generation")
    else
        osmcoastline = conf["default"]["osmcoastline"]
        coast_filter = conf["default"]["coast_filter"]
        read(`rm -f $intermediate $final`)   #if it exists
        osmdata = conf["default"]["osmdata"]
        read(`$coast_filter -o $intermediate $osmdata`)
        try
            read(`$osmcoastline -m 0 -o $final $intermediate`)
        catch
            println("Ignoring error return from osmcoastline")
        end
        read(`$(conf["default"]["ogr2ogr"]) -f GPKG $finalshape $final land_polygons`)
    end
    boundaries = conf["default"]["boundaries"]
    do_decode(ogr_decode,finalshape,"Default",boundaries,out_dir,0)
end

extract_landcovers(redo,conf;rawfiles=get_datafiles(conf),one_tile=conf["default"]["boundaries"]) = begin
    base,local_data,local_scratch = setup_tile(one_tile[2],one_tile[3],conf)
    local_datafiles = map(x->make_local_data(x,one_tile,local_scratch),rawfiles)
    println("Set up $one_tile for landcovers")
    only_landcovers = conf["default"]["only_landcovers"]
    for (landname,specs) in get_landcover_recipes(;restrict=only_landcovers)
        output_name = joinpath(local_data,landname * ".gpkg")
        println("Extracting $output_name...")
        extract_multi(redo,one_tile,local_scratch,local_datafiles,output_name,specs)
    end
end

# Changed to use viewfinder panorama data pre-downloaded
extract_elevations(redo,conf) = begin

    boundaries = conf["default"]["boundaries"]
    outdir = joinpath(workdir,"SRTM-3")
    if !redo && isdir(outdir)
        test_dir = joinpath(outdir,calculate_filenames(boundaries)[1:2]...)
        if isdir(test_dir)
            println("Directory $test_dir exists, skipping as redo not requested")
            return
        else
            println("Could not find $test_dir, extracting")
        end
    end
    rm(outdir,force=true,recursive=true)
    all_data = readdir(elevations)
    oldwd = pwd()
    cd(elevations)
    # read(`$hgtchop 3 $full_path $outdir`)
    # Fun fact: gdalchop wants all input files at once
    # otherwise lots of stuff gets overwritten with zeros
    println("Now executing $chopper $outdir $all_data")
    read(`$chopper $outdir $all_data`) 
    cd(oldwd)
    #
    # Now get rid of any dodgy points
    #
    fix_dodgy_points(outdir,50,true)
    installexedir = conf["default"]["installexedir"]
    ENV["LD_LIBRARY_PATH"] = "$installexedir/../lib"
    read(`$terrafit --minnodes 50 --maxnodes 20000 --maxerror 10 $outdir`)
end

create_airports(redo,conf;limits=[]) = begin
    if limits == [] limits = conf["default"]["boundaries"] end
    if isdir(joinpath(workdir,"AirportArea")) && !redo
        println("Skipping airport generation, already present")
        return
    end
    read(`$genapts --input=$(conf["default"]["airports"]) --work=$(workdir) --max-lat=$(limits[1]) --min-lat=$(limits[2]) --min-lon=$(limits[3]) --max-lon=$(limits[4]) --verbose --log-level=info`)
end

#
# Cliffs affect height calculations so must be done before airports are
# calculated. We also need to work with spatialite to remove dense cliff
# clusters.
#
construct_cliffs(redo,conf;limits=[]) = begin
    if limits == [] limits = conf["default"]["boundaries"] end
    have_new = false     #Indicates if we've generated new data
    final_gpkgfile = joinpath(scratchdir,"Cliffs.gpkg")
    if (!redo) && (isdir(final_gpkgfile) || isfile(final_gpkgfile))
        println("Skipping OSM cliff extraction, already present")
    else
        have_new = true
        osmdata = OSMData(conf["default"]["osmdata"])
        extract_cliffs(limits,scratchdir,osmdata,final_gpkgfile,redo=redo)
    end
    # Add cliffs to scenery
    cliffdir = joinpath(workdir,"Cliffs")
    if (!redo && !have_new) && isdir(cliffdir)
        println("Skipping cliff decode, already there")
    else
        do_decode(ogr_decode,final_gpkgfile,"Cliffs",limits,cliffdir,1,cliffs=true)
        have_new = true
    end
    # Add them also to elevations
    ymax,ymin,xmin,xmax = limits
    height_dir = joinpath(workdir,"SRTM-3")
    # Remove old cliff and rectified files
    have_cliffs = length(read(`find $height_dir -name "*.cliffs" -print`)) > 0
    if have_cliffs && (!redo && !have_new)
        println("Cliffs already present in directory, skipping")
        return
    end
    read(`find $height_dir -name "*.cliffs" -delete`)
    read(`$cliff_decode --all-threads --spat $xmin $ymin $xmax $ymax $height_dir $final_gpkgfile`)
    # And now rectify the heights
    read(`find $height_dir -name "*.rectified*" -delete`)
    read(`$rectify --work-dir=$(workdir) --height-dir=SRTM-3 --min-lon=$xmin --max-lon=$xmax --min-lat=$ymin --max-lat=$ymax --min-dist=50`)
    # And redo terrafit just in case
    installexedir = conf["default"]["installexedir"]
    ENV["LD_LIBRARY_PATH"] = "$installexedir/../lib"
    read(`$terrafit --force --minnodes 50 --maxnodes 20000 --maxerror 10 $height_dir`)
end

# Take the per-tile information and put it into the general work dir
make_areas(redo,conf;limits=[]) = begin
    if limits == [] limits = conf["default"]["boundaries"] end
    _,datadir,_ = setup_tile(limits[2],limits[3],conf)
    landtypes = get_land_types(conf["default"]["priorities"])
    target_files = readdir(datadir)
    filter!(t->splitext(t)[1] in landtypes, target_files)
    println("Found data for $target_files")
    for t in target_files
        full_path = joinpath(datadir,t)
        mod_time = mtime(full_path)
        output_dir=joinpath(workdir,splitext(t)[1])
        full_output_dir = joinpath(output_dir,tile_dirs(limits)...)
        if isdir(full_output_dir)
            #println("All files: $(readdir(full_output_dir))")
            all_times = map(x->mtime(joinpath(full_output_dir,x)),readdir(full_output_dir))
            latest = max(all_times...)
            #println("All mtimes: $all_times")
        else
            println("No directory: $full_output_dir")
            latest = 0.0
        end
        if latest < mod_time
            println("Decoding $t: landcover $mod_time, latest decoded $latest")
            rm(full_output_dir,force=true,recursive=true)
            linewidth = get_width(get_landcover_recipes()[splitext(t)[1]])
            do_decode(ogr_decode,full_path,splitext(t)[1],limits,output_dir,linewidth)
        end
    end
end

call_tgconstruct(redo,conf;limits=[]) = begin
    if limits == [] limits = conf["default"]["boundaries"] end
    n,s,w,e = limits
    root_dir = conf["default"]["root"]
    only_landcovers = conf["default"]["only_landcovers"]
    start_bot,start_left = conf["default"]["start_tile"]
    outputdir = joinpath(root_dir,"output","Terrain")
    # remove old working files
    rm(joinpath(workdir,"Shared"),force=true,recursive=true)
    if only_landcovers == "all" 
        all_files = readdir(workdir)
    else
        all_files = only_landcovers
        # Add in compulsory ones
        append!(all_files,["Default","AirportObj","AirportArea","Cliffs","SRTM-3"])
    end
    priorities = conf["default"]["priorities"]
    # Do 1x1 degree squares to avoid tgconstruct crashes
    for bot = s:(n-1)
        if bot < start_bot continue end
        for left = w:(e-1)
            if bot == start_bot && left < start_left continue end
            command = `$tgconstruct --threads --output-dir=$outputdir --work-dir=$(workdir) --max-lat=$(bot+1) --min-lat=$bot --min-lon=$left --max-lon=$(left+1) --priorities=$priorities $all_files`
            println(command)
            run(command)
        end
    end
end

provide_report(redo,conf) = begin
    # report configuration
    println("Configuration:")
    println("Root dir: $(conf["default"]["root"])")
    println("Exe dir: $(conf["default"]["installexedir"])")
    println("State data: ")
    for st in conf["default"]["state_list"]
        println("\n$(st.name)")
        println("$(conf[st.name]["state_data"])")
    end
        
    boundaries = conf["default"]["boundaries"]
    priorities = conf["default"]["priorities"]
    println("Scenery generation status\n")
    println("Current boundaries N, S, W, E: $boundaries")
    landcovers = get_land_types(priorities)
    println("All landcovers: $landcovers")
    defined_l = sort(collect(keys(get_landcover_recipes())))
    println("All defined: $defined_l")
    scenery_dir = conf["default"]["root"]
    for tile in make_tiles(boundaries)
        if !has_heights(tile)
            println("Water: $tile")
            continue
        end
        println("Tile: $tile")
        base,datadir,scratch = setup_tile(tile[2],tile[3],conf)
        target_files = readdir(datadir)
        filter!(t->t[1:(end-5)] in landcovers,target_files)
        println("Rawdata created in: $datadir")
        # check decoded status
        f,s = tile_dirs(tile)
        decode_dir = joinpath(scenery_dir,"work")
        decoded_files = readdir(decode_dir)
        filter!(d->d in landcovers,decoded_files)
        println("Data present (asterisk - decoded for terragear out of date or missing):")
        for t in target_files
            print("$t")
            decode_sub_dir = joinpath(decode_dir,t[1:(end-5)],f,s)
            if !(t in decoded_files) || mtime(decode_sub_dir) < mtime(joinpath(datadir,t))
                print("*")
            end
            print("\n")
        end
    end
end

"""
Remove all landcover files from scratch and work dir
"""
cleanup(redo,conf) = begin
    restrict = conf["default"]["only_landcovers"]
    for_cleanup = keys(get_landcover_recipes(restrict=restrict))
    boundaries = conf["default"]["boundaries"]
    # No more working directory
    rm.(joinpath.(workdir,for_cleanup),recursive=true,force=true)
    for tile in make_tiles(boundaries)
        if !has_heights(tile)
            continue
        end
        println("Cleaning $tile")
        base,datadir,scratch = setup_tile(tile[2],tile[3],conf)
        target_files = joinpath.(datadir,filter(x->x in for_cleanup,readdir(datadir)))
        println("Removing $target_files")
        rm.(target_files,recursive=true,force=true)
        # all scratch files...gone...in the future
    end 
end

"""

Create a sequence of 1x1degree tiles for construction.
"""
make_tiles(limits) = begin
    top,bottom,left,right = limits
    top = ceil(top)
    bottom = floor(bottom)
    left = floor(left)
    right = ceil(right)
    final_list = []
    for lat in bottom:1.0:top-0.1
        for long in left:1.0:right-0.1
            push!(final_list,[lat+1,lat,long,long+1])
        end
    end
    return final_list
end

tile_dirs(limits) = begin
    _,bottom,left,_ = limits
    if bottom < 0 latlet = 's' else latlet = 'n' end
    if left < 0 longlet = 'w' else longlet = 'e' end
    firstdir = "$longlet$(Int(abs(10*fld(left,10))))$latlet$(Int(abs(10*fld(bottom,10))))"
    seconddir = "$longlet$(Int(abs(left)))$latlet$(Int(abs(bottom)))"
    return firstdir,seconddir
end
                               
has_heights(limits) = begin
    top_level = joinpath(workdir,"SRTM-3")
    firstdir,seconddir = tile_dirs(limits)
    println("Testing $firstdir/$seconddir for water")
    if !isdir(joinpath(top_level,firstdir))
        println("No directory $firstdir, no heights")
        return false
    end
    full_loc = joinpath(top_level,firstdir,seconddir)
    if !isdir(full_loc)
        println("No directory $full_loc, no heights")
        return false
    end
    # any non-trivial files here?
    all_files = filter(x->filesize(joinpath(full_loc,x))>500,readdir(full_loc))
    #println("All non-flat areas $all_files")
    if length(all_files) == 0
        println("No significant files in $full_loc")
    end
    
    return length(all_files)>0
end
    
#== Do a full run. This differs from the individual steps by dividing the target area into
smaller pieces. ==#

master_run(redo,conf) = begin
    make_coastline(redo,conf)         #use global boundaries
    extract_elevations(redo,conf)     #use global boundaries
    construct_cliffs(redo,conf)
    create_airports(redo,conf)
    states = conf["default"]["state_list"]
    start_corner = conf["default"]["start_tile"]
    for one_tile in make_tiles(conf["default"]["boundaries"])
        if one_tile[2] < start_corner[1]
	    println("Skipping $one_tile, less than $start_corner")
	    continue
	    end
        if one_tile[2] == start_corner[1] && one_tile[3] < start_corner[2]
            println("Skipping $one_tile, less than start $start_corner")
            continue
        end
        if !has_heights(one_tile)
            println("Skipping $one_tile, no heights")
            continue
        end
        
        rawfiles = get_datafiles(states,one_tile)
        push!(rawfiles,OSMData(conf["default"]["osmdata"]))
        extract_landcovers(redo,conf,rawfiles=rawfiles,one_tile=one_tile)
        make_areas(redo,conf,limits=one_tile)
        println("\nFinished $one_tile\n")
    end
    call_tgconstruct(redo,conf)
    try   #no shame in failing this
        make_construction_report(conf)
    catch
    end
end

"""
This function will create a file at the specified location that tries to provide
enough information to reproduce the scenery build
"""
make_construction_report(conf) = begin
    outfile = joinpath(conf["default"]["root"],"output","construct_log")
    finished = "$(Dates.now())"
    curr_dir = pwd()
    exe_dir = dirname(realpath(@__FILE__))
    cd(exe_dir)
    git_info = "Unknown git version"
    try
        git_info = read(pipeline(`git log -n 1 HEAD`,`head -n 1`),String)
    catch SystemError
    end
    cd(curr_dir)
    osm_data = conf["default"]["osmdata"]
    osm_date = "$(Dates.unix2datetime(ctime(osm_data)))"
    osm_md5 = read(`md5sum -b $osm_data`,String)[1:32]
    apt_date = "$(Dates.unix2datetime(ctime(conf["default"]["airports"])))"
    apt_sum = read(`md5sum -t $(conf["default"]["airports"])`,String)[1:32]
    config_contents = read(conf["default"]["source_file"],String)
    priorities = read(conf["default"]["priorities"],String)
    of = open(outfile,"w")
    write(of,"# Scenery constructed on $finished\n# Scenery tools git $git_info\n",
             "# OSM data $osm_date md5sum $osm_md5\n",
             "# Apt.dat generated $apt_date md5sum $apt_sum\n",
             "# ====== Config file contents ===== \n $config_contents\n",
             "# ====== End config file ====== \n",
          "# ====== Priorities ====== \n$priorities\n")
    close(of)
end

# The possible things to do
const actions = Dict("coastline"=>make_coastline,
                     "landcovers"=>extract_landcovers,
                     "elevations"=>extract_elevations,
                     "airports"=>create_airports,
               "cliffs"=>construct_cliffs,
               "areas"=>make_areas,
                     "construct"=>call_tgconstruct,
                     "report"=>provide_report,
                     "all"=>master_run,
                     "clean"=>cleanup
               )


help_message() = begin
    mess = """
Usage: julia scenery_maker.jl [config] [ACTION] [redo]

Config is the path to a configuration file. The configuration file
defines (in ini syntax) the values of a number of global variables.
Sections in the .ini file refer to separate states. Please refer to
the supplied example file.

If redo is false, any existing data is not regenerated. If true, pre-existing
work is deleted before generating.

Action can be:

coastline:   Generate coastline.
landcovers:  Extract all defined landcovers from OSM,ALUM,NVIS data into shapefiles
elevations:  Generate elevation files from SRTM-3 files
cliffs:      Extract cliff information and rectify heights
airports:    Run genapt850 to add airports
areas:       Run ogr-decode to turn landcover information into terragear polygons
construct:   Run tg-construct on all areas that are available and recognised
all:         Do everything, usually in 1x1 degree blocks
report:      Report detailed status
clean:       Remove all landcover files (coastline,elevations,cliffs,airports remain)

To generate scenery from scratch not using 'all', you should run the tasks in the above order
(except for report/clean of course).

Currently defined areas are:
"""
    println(mess)
    k = sort(collect(keys(get_landcover_recipes())))
    for kk in k
        println(kk)
    end
    
end

# We expand out directory names to save doing it later, and transfer everything
# into a plain dictionary
read_config(filename) = begin
    conf = ConfParse(filename)
    parse_conf!(conf)
    our_conf = Dict{String,Dict}()
    for k in keys(conf._data)
        our_conf[k] = Dict{String,Any}()
        for k2 in keys(conf._data[k])
            our_conf[k][k2] = retrieve(conf,k,k2)
        end
    end
    scenery_top = retrieve(conf,"default","scenery_top")
    project_top = retrieve(conf,"default","project_top")
    root_dir = joinpath(scenery_top,project_top)
    our_conf["default"]["root"]=root_dir
    installexedir = retrieve(conf,"default","installexedir")
    priorities = retrieve(conf,"default","priorities")
    if first(priorities) != '/'
        our_conf["default"]["priorities"] = joinpath(installexedir,priorities)
    end
    coast_filter = retrieve(conf,"default","coast_filter")
    if first(coast_filter) != '/'
        our_conf["default"]["coast_filter"] = joinpath(installexedir,coast_filter)
    end
    osmcoastline = retrieve(conf,"default","osmcoastline")
    if first(osmcoastline) != '/'
        our_conf["default"]["osmcoastline"] = joinpath(installexedir,osmcoastline)
    end
    global_dir = joinpath(scenery_top,retrieve(conf,"default","global_dir"))
    our_conf["default"]["global_dir"] = global_dir
    our_conf["default"]["airports"] = joinpath(global_dir,retrieve(conf,"default","airports"))
    our_conf["default"]["osmdata"] = joinpath(global_dir,retrieve(conf,"default","osmdata"))
    our_conf["default"]["start_tile"] = parse.(Int32,
                                               get(our_conf["default"],"start_tile",
                                                   our_conf["default"]["boundaries"][2:3]))
    our_conf["default"]["boundaries"] = parse.(Int32,our_conf["default"]["boundaries"])
    # store the filename for our records
    our_conf["default"]["source_file"] = filename
    # Now the states
    states = keys(our_conf)
    state_list = []
    println("Config before states: $our_conf")
    for st in states
        if st == "default" continue end
        data_files = DataSource[]
        shapefile = joinpath(global_dir,retrieve(conf,st,"shapefile"))
        inner_rect = parse.(Int32,retrieve(conf,st,"inner_rect"))
        push!(data_files,ALUMData(our_conf,st))
        append!(data_files,NVISData(our_conf,st))
        this_state = State(data_files,inner_rect,st,shapefile,retrieve(conf,st,"state_layer"))
        our_conf[st]["state_data"] = this_state
        push!(state_list,this_state)
    end
    our_conf["default"]["state_list"] = state_list
    return our_conf
end

main_prog() = begin
    if length(ARGS) == 0
        help_message()
        exit()
    end
    
    config = joinpath(pwd(),ARGS[1])

    conf = read_config(config)
    setup(conf)
    
    action = ARGS[2]
    redo = false
    if length(ARGS)==3
        redo = ARGS[3][1] =='t'
    end
    actions[action](redo,conf)
end

if abspath(PROGRAM_FILE) == @__FILE__
    main_prog()
end


#==

Installation
============

This script essentially orchestrates running a variety of
software that actually does the work. You should have the
following installed, together with their prerequisites:

(1) Julia (download from julialang.org)
(2) Grass (download from grass.osgeo.org, or use Linux package)
(3) osmcoastline (https://github.com/osmcode/osmcoastline)
(4) Terragear toolchain (see flightgear documentation)
(5) ogr2ogr (part of gdal, see gdal.org or use Linux package)
(6) osmosis (https://wiki.openstreetmap.org/wiki/Osmosis or Linux package)
(7) grep (available on Linux,Mac, Windows?)

Place this file and the file SceneryTools.jl in the same directory. That
is the end of installation.

In addition to this you will need an OpenStreetMap *.pbf file for the
area you are working on and a file in GeoDataBase format (.gdb) for
the area you are working on (see sample configuration).

Usage
=====

Type "julia scenery_maker.jl" at the command line for instructions.
You should be in the same directory as "scenery_maker.jl" for this
to work.

==#
