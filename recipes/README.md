# Recipes for constructing (Australian) scenery

The recipes in this directory can be used to automate
scenery construction.  Please include instructions in
the files themselves. Text guides are also welcome.

## Available recipes

* scenery_maker.jl: A Julia script generating scenery from
  Geodatabase files and OpenStreetMaps data
  
