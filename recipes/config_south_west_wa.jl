#
# Configuration
# =============
#

# The root of your scenery directories, no changes are made outside this directory
const scenery_top = "/home/jrh/programs/Games/FlightGear/CustomScenery"

# Boundaries of extracted area:2x2 deg south-west
# const boundaries = [-33,-35,114.9,117] # Boundaries of generated scenery
# South-west test area
const boundaries = [-33.5,-34,116,116.5]
# Terragear executable locations
const installexedir = "/home/jrh/programs/Games/FlightGear/next/install/bin"

# Path to default_priorities.txt, below assumes normal terragear installation
const priorities = joinpath(installexedir,"..","share","TerraGear","default_priorities.txt")

# Some non-flightgear executables
const ogr2ogr = "ogr2ogr"  #in the system path, no directory needed
const coast_filter = "$scenery_top/osmcoastline/src/osmcoastline_filter"
const osmcoastline = "$scenery_top/osmcoastline/src/osmcoastline"

# Per-project configuration
const project_top = "SouthWestWA"    # directory name
const landuse_data = "$(joinpath(scenery_top,project_top,"rawdata/WA_CLUM_August 2018/WA_Landuse_August2018.gdb"))"   # geodatabase data
const landuse_layer = "WA_CLUM_August2018"    # Name of layer in landuse_data geodatabase
# Name of field containing ALUM data
const alum_field = "alum"
# Location of NVIS major vegetation subgroup geodatabase file
const vegecover_data = joinpath(scenery_top,project_top,"rawdata/VegeCover/NVIS5_1_AUST_EXT_WA.gdb")
# Name of layer to use in vegecover_data
const vegecover_layer = "nvis5_1_aust_ext_wa"
# Name of key for matching major vegetation group with id
const vegecover_key = joinpath(scenery_top,project_top,"rawdata/VegeCover/NVIS_5_1_LUT_AUST_FLAT.gdb")
# Name of layer in vegecover_key to use for matching
const vegecover_key_layer = "nvis5_1_lut_aust_flat"
# Location of openstreetmap pbf file
const osmdata = joinpath(scenery_top,"GlobalData","australia-latest.osm.pbf") # OSM planet file
# Where to download missing elevation data
const elevation_source = "https://dds.cr.usgs.gov/srtm/version2_1/SRTM3/Australia/" 
# Source of airport information
const airports = joinpath(scenery_top,"GlobalData","apt.dat") #airport information


