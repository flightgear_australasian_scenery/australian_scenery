# Search for heights that are more than a given number of metres different from
# all of their neighbours

# Calculate the appropriate filenames from a
# (long,lat) pair
using GZip
using Printf

bucket_corner(bname) = begin
    bbody = match(r"[0-9]+",bname).match
    bbody = parse(Int,bbody)
    long_val = bbody >> 14
    remain = bbody - (long_val << 14)
    lat_val = (remain >> 6)
    remain = remain - lat_val << 6
    y = remain >> 3
    x = remain - y << 3
    # see table in FlightGear newbucket.hxx for long divs
    yspan = 0.125
    if abs(lat_val-90) < 22 xspan = 0.125
    elseif abs(lat_val-90) < 62 xspan = 0.25
    else
        println("Got long,lat $long_val $lat_val $x $y")
        error("Please add xspans for latitudes > $lat_val")
    end
    return (long_val-180)+xspan*x,(lat_val-90)+yspan*y,xspan,yspan
end

point_to_latlong(bname,row,col) = begin
    corn_long,corn_lat,_,_ = bucket_corner(bname)
    long = corn_long + 3/3600 * (col-1)
    lat = corn_lat + 3/3600 * (row-1)
    return long,lat
end

"""
Span in seconds of arc. We need to return the total number of
        rows in order to calculate the grid index within the bucket later.
"""
load_heights(filename) = begin
    f = gzopen(filename)
    header = read(f,UInt32)
    if header != 0x54474152
        error(@sprintf "Height file has incorrect header %x" header)
    end
    minx = read(f,Int32)
    miny = read(f,Int32)
    cols = read(f,Int32)
    colstep = read(f,Int32)
    rows = read(f,Int32)
    rowstep = read(f,Int32)
    #println("$cols x $rows starting at $(minx/3600), $(miny/3600) step $colstep, $rowstep")
    heights = read(f,Matrix{Int16}(undef,rows,cols)) #row-major order! True story!
close(f)
    return heights
end

find_dodgy_points(hts,max_dev) = begin
    rows = size(hts)[1]
    cols = size(hts)[2]
    zero_cnt = 0
    dodgy_points = Array{Int}[]
    for r in 2:(rows-1)
        prev_col = hts[r,1]
        for c in 2:(cols-1)
            centre_ht = hts[r,c]
            # only correct zeros
            if centre_ht != 0
                prev_col = centre_ht
                continue
            end
            zero_cnt += 1
            # Compare with ht at left
            if abs(centre_ht-prev_col)<max_dev
                prev_col = centre_ht
                continue
            end
            prev_col = centre_ht #for next time through
            # Compare with hts above and below
            if abs(hts[r-1,c-1]-centre_ht)<max_dev continue end
            if abs(hts[r-1,c]-centre_ht)<max_dev continue end
            if abs(hts[r-1,c+1]-centre_ht)<max_dev continue end
            if abs(hts[r+1,c-1]-centre_ht)<max_dev continue end
            if abs(hts[r+1,c]-centre_ht)<max_dev continue end
            if abs(hts[r+1,c+1]-centre_ht)<max_dev continue end
            # And the next point
            if abs(hts[r,c+1]-centre_ht)<max_dev continue end
            # If we get here we have a winner
            push!(dodgy_points,[r,c,centre_ht])
        end
    end
    #println("Zero heights: $zero_cnt")
    return dodgy_points
end

fix_heights(dodgy_pts,hts) = begin
    for (r,c,_) in dodgy_pts
        ave = round(Int,(hts[r-1,c-1]+hts[r-1,c]+hts[r-1,c+1] +
            hts[r+1,c-1]+hts[r+1,c]+hts[r+1,c+1] +
                    hts[r,c-1] + hts[r,c+1])/8)
        hts[r,c] = ave
        println("New height for $r $c is $ave")
    end
    return hts
end

"""
Output a height array. Old array is in file ending ".old"
"""
write_fixed_points(bucket,hts) = begin
in_file = missing
out_file = missing
    try
    in_file = gzopen(bucket)
    out_file = gzopen(bucket[1:(end-6)]*".arr.corrected.gz","w")
catch
close(in_file)
error("Failed to open $bucket for reading/$bucket.arr.correct.gz for writing")
end
    header = read(in_file,UInt32)
    if header != 0x54474152
        error(@sprintf "Height file has incorrect header %x" header)
    end
    minx = read(in_file,Int32)
    miny = read(in_file,Int32)
    cols = read(in_file,Int32)
    colstep = read(in_file,Int32)
    rows = read(in_file,Int32)
    rowstep = read(in_file,Int32)
    write(out_file,header,minx,miny,cols,colstep,rows,rowstep)
    for c in 1:cols
        for r in 1:rows
            write(out_file,hts[r,c])
        end
    end
    close(out_file)
    close(in_file)
    # mv(bucket,bucket*".old")
    mv(bucket[1:(end-6)]*".arr.corrected.gz",bucket;force=true)
end

command_line_use() = begin
    if length(ARGS) < 3
        println("Usage:")
        println("dodgy_point_search <workdir> min_dev fix")
        println("Max_dev is the minimum difference from the surrounding points needed")
        println("to consider that a height is bad.")
        println("If fix is true, recalculate the height")
        exit()
    end
    workdir = joinpath(pwd(),ARGS[1])
    min_dev = parse(Int,ARGS[2])
    fix = parse(Bool,ARGS[3])
    fix_dodgy_points(workdir,min_dev,fix)
end

fix_dodgy_points(workdir,min_dev,fix) = begin
    # Get a list of files

    println("Now fixing files in $workdir, min deviation $min_dev")
    for (root,dirs,files) in walkdir(workdir)
        for one_file in files
            if one_file[(end-6):end] != ".arr.gz" continue end
            bucket = joinpath(root,one_file)
	    heights = missing
	    try
            heights = load_heights(bucket)
	    catch
	    println("Failed to load heights for $bucket,ignoring")
	    continue
	    end
            # println("Processing $bucket")
            # println("Heights file has dimensions $(size(heights))")
            dodgy = find_dodgy_points(heights,min_dev)
            if length(dodgy) > 0
                print("$one_file : ")
                println("$(length(dodgy)) points")
                for (r,c,ht) in dodgy
                    lat,long = point_to_latlong(one_file,r,c)
                    println("$r $c $ht -> $lat $long")
                end
                if fix
                    fix_heights(dodgy,heights)
                    write_fixed_points(bucket,heights)
                end
            end
        end
    end
end
