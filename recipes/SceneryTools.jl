#== 

A collection of methods that manipulate geo data.

==#

export extract_from_alum    #create a shapefile from Australian ALUM gdb files
export extract_from_osm     #create a shapefile from OSM data
export merge_gisfiles       #merge multiple GIS files into a single file

"""
The LandCover type contains information needed to extract landcover information
from a particular data type. All landcovers should have an 'outfile' field which
will be the name of the shapefile output after extraction.

When a new DataSource is added, a new LandCover subtype can also be added, and
``data_type(::YourNewLandCover)`` should return the data source type.
""" 
abstract type LandCover end

get_outfile_name(l::LandCover) = l.outfile

struct OSM <: LandCover
    phrase::String
    relation::Bool
    make_area::Bool
    outfile::String
end

data_type(::OSM) = OSMData

is_relation(o::OSM) = o.relation

struct ALUM <: LandCover
    codes::Vector{Int64}
    outfile::String
    buffer::Float64   #expand by this much
end

ALUM(codes,outfile) = ALUM(codes,outfile,0.0)

data_type(::ALUM) = ALUMData

make_alum(alum_style) = begin
    if alum_style == "dotted"
        return x -> begin a = join("$x","."); return "'$a'" end
    else
        return x -> "'$x'"
    end
end

struct NVIS <: LandCover
    phrase::String
    outfile::String
end

data_type(::NVIS) = NVISData

"""
Turn the list of alum codes into a search string. If all codes end in '0', the final
digit is replaced by the digits 1-9.
"""
make_alum_string(alum_list,format_alum,alum_field) = begin
    full_list = []
    dont_expand = any(x->x%10!=0,alum_list)
    if dont_expand full_list = alum_list
    else
        println("Expanding alum codes for $alum_list")
        for a in alum_list
            base = "$a"
            push!(full_list,base)
            tertiary = "$a"[end]
            if tertiary == '0'
                for i in 1:9   #all subclasses
                    push!(full_list,"$(a+i)")
                end
            end
        end
    end
    full_string = ""
    test_len = length(format_alum("123"))-2
    bits = map(x -> "SUBSTR(cast($alum_field as text),1,$test_len) = $(format_alum(x))",full_list)
    full_string = join(bits," OR ")
    return full_string
end

"""
A DataSource describes a class of geospatial data. It includes any
information needed to interact and extract landcover information.
get_filename(datasource) should return a filename.

A constructor should be provided to create a datasource from
config file information.
"""
abstract type DataSource end

get_filename(d::DataSource) = d.filename

struct OSMData <: DataSource
    filename::String
end

struct ALUMData <: DataSource
    filename::String
    layer::String
    field_name::String  #name of field holding data
    style::String #"dotted" or "not_dotted"
    is_pixelated::Bool
    geom_col::String #which column holds the geometry
end

# remove unused ALUM codes, could be derived later
const  unused_alum_codes = [110,111,112,113,114,115,116,117,
                            120,122,124,125,130,131,132,133,
                            210,220]

# ALUM data can be in shapefile format, which is annoying because
# there is no geometry column. So we take the precaution of convertin
# such files to sqlite immediately.
ALUMData(c,st::String) = begin
    root_dir = c["default"]["global_dir"]
    landuse_data = joinpath(root_dir,c[st]["landuse_data"])
    is_pixelated = c[st]["alum_is_pixelated"] == "true"
    pre_process = get(c[st],"alum_preprocess","")
    # ini files can't handle underscores...
    pre_process = replace(pre_process,"~"=>" ")
    if get_gis_file_type(landuse_data) == "ESRI"  #shapefile
        new_file = landuse_data*".sql"
        if !ispath(new_file)
            println("$new_file does not exist: creating")
            run(`ogr2ogr -overwrite -f 'SQLite' -dim XY -dsco SPATIALITE=YES -nlt PROMOTE_TO_MULTI $new_file $landuse_data`)
            prune_alum(new_file,landuse_data,c[st]["alum_field"],unused_alum_codes)
            if pre_process != ""
                println("Running $pre_process")
                s = read(`spatialite $new_file $pre_process`,String)
                println("Result of preprocess is $s")
            end
            read(`spatialite $new_file "create table my_extent (id varchar,n int, s int, w int, e int);"`)
            read(`spatialite $new_file "insert into my_extent values ('current',0,0,0,0);"`)
        end
        c[st]["landuse_data"] = new_file
        landuse_data = new_file
    end
    geom_col = get_geom_col(landuse_data,c[st]["landuse_layer"])
    ALUMData(landuse_data,c[st]["landuse_layer"],
             c[st]["alum_field"],
             c[st]["alum_style"],
             is_pixelated,
             geom_col)
end

struct NVISData <: DataSource
    filename::String
    layer::String
    key::String
    key_filename::String  # if kept in a separate file
end

NVISData(c,st::String) = begin
    root_dir = c["default"]["global_dir"]
    if !haskey(c[st],"vegecover_data") return [] end
    vegecover_data = joinpath(root_dir,c[st]["vegecover_data"])
    key_filename = joinpath(root_dir,c[st]["vegecover_key"])
    [NVISData(vegecover_data,c[st]["vegecover_layer"],
             c[st]["vegecover_key_layer"],key_filename)]
end

# A state has an inner bounding rectangle - any area within this
# is guaranteed to be fully in the state. Otherwise, the shapefile
# contains the full state shape.

"""
A State is an area with a set of data files, a bounding shapefile
and an inner rectangle for quick determination as to whether a
given area lies fully in the state
"""
struct State
    datafiles::Vector{DataSource}
    inner_boundary::Vector{Int32} #rectangle fully in state
    name::String
    full_shapefile::String #location of shapefile
    state_layer::String# layer in shapefile
end

is_all_in_state(s::State,r) = begin
    sn,ss,sw,se = s.inner_boundary
    n,s,w,e = r
    return n < sn && s > ss && w > sw && e < se
end

is_part_in_state(st::State,r) =  begin
    n,s,w,e = r
    # use the shapefile, Luke
    topo_dat = read(`ogrinfo -so -spat $w $s $e $n $(st.full_shapefile) $(st.state_layer)`,String)
    numareas = match(r".*Feature Count: ([0-9]+)",topo_dat)[1]
    return numareas != "0"
end

struct LandConfig
    width::Int
    smooth::Bool
    spec::Array{LandCover,1}
end

get_width(r::LandConfig) = r.width
get_recipes(r::LandConfig) = r.spec
get_line(r::LandConfig) = [r.spec[1]]
smoothing(r::LandConfig) = r.smooth

# Get all data sources that are required to generate land covers for this
# rectangle.
get_datafiles(states,rect) = begin
    for st in states
        if is_all_in_state(st,rect)
            println("$rect is in $(st.name)")
            return copy(st.datafiles)
        end
    end
    all_datafiles = []
    for st in states
        if is_part_in_state(st,rect)
            println("$rect overlaps $(st.name)")
            append!(all_datafiles,st.datafiles)
        end
    end
    return all_datafiles
end

# Get the relevant datafiles for the given configuration
get_datafiles(conf) = begin
    rect = conf["default"]["boundaries"]
    states = conf["default"]["state_list"]
    local_files = get_datafiles(states,rect)
    push!(local_files,OSMData(conf["default"]["osmdata"]))
end

#Make the local data files for each type of data input, returning the file name
make_local_data(d::DataSource,boundaries,rawdir) = begin
    n,s,w,e = boundaries
    error("$(typeof(d)) does not have make_local_data defined; please do this")
end

make_local_data(d::ALUMData,boundaries,rawdir) = begin
    n,s,w,e = boundaries
    fullname = get_filename(d)
    layer = d.layer
    geom_col = d.geom_col
    fname = splitpath(fullname)[end]
    source_dir = dirname(fullname)
    if fname[(end-2):end] != "sql"
        master_db =  joinpath(source_dir,fname*".sql")
        if !isfile(master_db)
            println("No sign of $master_db, creating")
            read(`ogr2ogr -f SQLite -dim XY -dsco SPATIALITE=YES -nlt PROMOTE_TO_MULTI $master_db $fullname`) #create SQLite db
            read(`spatialite $master_db "create table my_extent (id varchar,n int, s int, w int, e int);"`)
            read(`spatialite $master_db "insert into my_extent values ('current',0,0,0,0);"`)
        end
    else
        master_db = fullname
    end
    # check for local information
    bounds = read(`spatialite -silent $master_db "select n,s,w,e from my_extent where id = 'current'"`,String)
    ln,ls,lw,le = parse.(Int,split(bounds,"|"))
    if ln!=n || ls !=s || lw != w || le !=e
        println("ALUM: Replacing old $bounds with new $boundaries")
        try
            read(`spatialite $master_db "drop table local_table"`)
        catch
            println("Error dropping tables; continuing")
        end
        # Get SRID
        result = read(`spatialite -silent $master_db "select srid from geometry_columns where f_table_name = lower('$layer')"`,String)
        println("We have a result: $result")
        srid = parse(Int32,result)
        println("Original database has SRID $srid")
        read(`spatialite $master_db "create table local_table as select $geom_col as local_geom,$(d.field_name) as local_field from $layer where 
              mbrintersects($geom_col,
              ST_Transform(ST_PolyFromText('polygon (($w $s, $e $s, $e $n, $w $n, $w $s ))',4283),$srid))"`)
        read(`spatialite $master_db "select recovergeometrycolumn('local_table','local_geom',$srid,'MULTIPOLYGON')"`)
        read(`spatialite $master_db "update my_extent set n=$n, s=$s, w=$w, e=$e where id = 'current'"`)
        println("Finished updating ALUM database $master_db for area $boundaries")
    end
    return ALUMData(master_db,"local_table","local_field",d.style,d.is_pixelated,"local_geom")
end

extract_landcover(ltype::ALUM,boundaries,rawdir,local_data::ALUMData,output_name;linear=false,smooth=false) = begin
    println("Generating $output_name")
    n,s,w,e = boundaries
    temp_filename = tempname()
    tid = Threads.threadid()
    format_alum = make_alum(local_data.style)
    alum_field = local_data.field_name
    phrase = make_alum_string(ltype.codes,format_alum,alum_field)
    small_bit = get_filename(local_data)
    command = `ogr2ogr -f GPKG  -where $phrase -simplify 0.00001 -select $alum_field -mapfieldtype Binary=String -t_srs WGS84 -nln multipolygons $temp_filename $small_bit $(local_data.layer)`
    println(command)
    read(command)
    if !has_features(temp_filename) return nothing end
    rm(output_name,force=true,recursive=true)
    if smooth snap = -1 else snap = 1e-09 end #WA suburb data needs snapping
    buffer = ltype.buffer
    # Now for some nicer smoothing
    sname,gscript = mktemp()
    write(gscript,"#!/bin/bash\n")
    write(gscript,"g.proj -c proj4=\"+proj=longlat +datum=WGS84 +no_defs\"\n")
    # Avoid weird leftover stuff after errors
    write(gscript,"g.remove -f type=vector pattern=\"temp_map_?_$tid\"\n")
    write(gscript,"v.in.ogr --overwrite -o snap=$snap input=$temp_filename output=temp_map_0_$tid\n")
    write(gscript,"g.region e=$e w=$w s=$s n=$n res=100\n")
    # Add a dummy column for dissolving
    write(gscript,"v.db.addcolumn map=temp_map_0_$tid columns='istree integer'\n")
    write(gscript,"v.db.update map=temp_map_0_$tid column=istree value=1\n")
    if smooth
        write(gscript,"v.dissolve input=temp_map_0_$tid column=istree output=temp_map_1_$tid --overwrite\n")
        write(gscript,"v.generalize input=temp_map_1_$tid method=chaiken threshold=0.0004 look_ahead=7 reduction=50 -l output=temp_map_2_$tid --overwrite\n")
    else
        write(gscript,"v.dissolve input=temp_map_0_$tid column=istree output=temp_map_2_$tid --overwrite\n")
    end
    # Buffer if needed
    if buffer > 0
        write(gscript,"v.buffer --overwrite input=temp_map_2_$tid output=temp_map_buffed distance=$buffer\n")
        write(gscript,"g.rename --overwrite vector=temp_map_buffed,temp_map_2_$tid\n")
    end
    write(gscript,"v.clean input=temp_map_2_$tid output=temp_map_3_$tid tool=rmarea threshold=5000.0 --overwrite\n")
    write(gscript,"v.out.ogr --overwrite input=temp_map_3_$tid type=auto output=$output_name format=GPKG output_layer=multipolygons")
    close(gscript)
    read(`grass --exec sh $sname`)
    return output_name
end

#==
# With OSM we first have to filter on what we want, then convert to GIS files. ``Is_relation`` is
# true if we are extracting a relation and false if we are extracting a way.
# id is a string identifying this extraction, and shapefile is the final file. ``make_area`` is
# true if we should turn any closed lines into areas

# It looks like ogr2ogr is not good at preserving inner areas, so we explicitly extract
# both outer and inner, and then subtract the inner areas when we have a relation.
#
# We get really funky with grep but it works pretty well!
#
==#
make_local_data(d::OSMData,boundaries,rawdir) = begin
    local_osm = joinpath(rawdir,"local_osm.pbf")  #Assume only one OSM source
    if !isfile(local_osm)
        osmdata = get_filename(d)
        println("Making local osm file $local_osm with boundaries $boundaries")
        run(`osmosis --read-pbf $osmdata --bounding-box top=$(boundaries[1]) bottom=$(boundaries[2]) left=$(boundaries[3]) right=$(boundaries[4]) completeWays=yes completeRelations=yes --write-pbf $local_osm`)
    end
    return OSMData(local_osm)
end

#
# Geometries extracted from OSM may be relations, polygons or linestrings. The latter may actually
# be closed polygons, but this is not always registered as such, so we make sure to do two separate
# processing passes on the OSM file to get both.
#
# Additionally we use this to extract streams and roads, which are linestrings that should not be
# turned into areas.
#
extract_landcover(ltype::OSM,boundaries,rawdir,local_data::OSMData,final_shapefile;linear=false,smooth=false) = begin
    name_base = get_outfile_name(ltype)
    filtered_name = joinpath(rawdir,name_base*".osm")
    println("Generating $final_shapefile")
    master_osm = get_filename(local_data)
    command = "osmosis --read-pbf $master_osm --lp "
    if !is_relation(ltype)
        command = command * "--tf accept-ways $(ltype.phrase) --tf reject-relations "
        command = command * "--used-node --write-xml $filtered_name" 
    else
        command = command * "--tf accept-relations $(ltype.phrase) --used-way "
        command = command * "--used-node --write-xml $filtered_name" 
    end
    # remove old files
    println("Removing $filtered_name")
    rm(filtered_name,force=true)
    # split command into pieces for the actual command
    split_command = split(command)
    cm = `$split_command`
    println(cm)
    read(cm)
    # Now post-process
    stage1file = joinpath(rawdir,name_base*"stage1")
    stage2file = joinpath(rawdir,name_base*"stage2")
    stage3file = joinpath(rawdir,name_base*"stage3")
    println("Removing old files")
    rm(stage1file,force=true,recursive=true)
    rm(stage2file,force=true,recursive=true)
    rm(stage3file,force=true,recursive=true)
    rm(final_shapefile,force=true,recursive=true)
    # Now we have extracted the file we process relations, linestrings and polygons separately
    # Now do our fancy inner-outer split
    if is_relation(ltype)
        extract_osm_relation(filtered_name,final_shapefile)
    else
        if !linear
            # get the polygons
            command = `ogr2ogr -where "OGR_GEOMETRY='MultiPolygon'" -skipfailures -f GPKG $stage1file $filtered_name`
            println(command)
            read(command)
        end
        # get the linestrings
        command = `ogr2ogr -where "OGR_GEOMETRY='LineString'" -skipfailures -f GPKG $stage2file $filtered_name`
        println(command)
        read(command)

        # Change lines to polygons if necessary
        println("For $final_shapefile make_area is $(ltype.make_area)")
        if ltype.make_area && has_features(stage2file)
            println("Transforming to areas $stage2file -> $stage3file")
            lines_to_areas(stage2file,stage3file)
        end

        # The final logic
        # So many things can go wrong, so we check at each step that we have what we
        # think we have. If it is all going pear-shaped, we return empty-handed.

        if linear
            if has_features(stage2file)
                mv(stage2file,final_shapefile)
                return final_shapefile
            else return nothing
            end
        end
        if has_features(stage3file) merge_gisfiles([stage1file,stage3file],final_shapefile,boundaries)
        else mv(stage1file,final_shapefile)
        end   
    end
    return final_shapefile
end

# To do this we need to be quite fancy with grep
# We convert both inner and outer to shape files before cutting out as otherwise grass seems
# to silently fail with anything unusual in the osm file.
extract_osm_relation(start_file,finish_file) = begin
    only_relations = `osmosis --read-xml - --lp --tf accept-relations --used-way --used-node --write-xml -`
    inner_scratch = start_file*"inner"*".osm"
    outer_scratch = start_file*"outer"*".osm"
    run(pipeline(`grep -v "role=\"outer\"" $start_file`,only_relations,inner_scratch))
    run(pipeline(`grep -v "role=\"inner\"" $start_file`,only_relations,outer_scratch))
    # make inner into shapefiles so we can count the features and find mistakes
    run(`ogr2ogr -overwrite -where "OGR_GEOMETRY='MultiPolygon'" -skipfailures -f 'ESRI Shapefile' -lco SHPT=POLYGON $(inner_scratch*".gpkg") $inner_scratch`)
    if has_features(inner_scratch*".gpkg")
        println("Cutting out islands from outer shape $outer_scratch - $inner_scratch")
        run(`ogr2ogr -overwrite -where "OGR_GEOMETRY='MultiPolygon'" -skipfailures -f GPKG $(outer_scratch*".gpkg") $outer_scratch`)
        if !has_features(outer_scratch*".gpkg")
            throw(error("File $(outer_scratch*".gpkg") has zero features instead of at least 1"))
        end
        cut_out(outer_scratch*".gpkg","multipolygons",inner_scratch*".gpkg","multipolygons",finish_file)
    else
        println("No inner features found for $finish_file")
        run(`ogr2ogr -where "OGR_GEOMETRY='MultiPolygon'" -skipfailures -f GPKG -overwrite $finish_file $outer_scratch`)
    end
end

#
# Gives the same results as extract_landcover, but detours through spatialite to cull cliffs
# that are too close together to give good results
#
const bad_cliff_ids = [527160466, 527162091]

extract_cliffs(boundaries,rawdir,local_data::OSMData,final_shapefile; redo=false) = begin
    ltype = OSM("natural=cliff",false,false,"Cliffs")
    name_base = "Cliffs"
    filtered_name = joinpath(rawdir,name_base*".osm")
    if !isfile(filtered_name) || redo
        println("Generating cliff spatialite file")
        master_osm = get_filename(local_data)
        command = "osmosis --read-pbf $master_osm --lp "
        command = command * "--tf accept-ways natural=cliff --tf reject-relations "
        command = command * "--used-node --write-xml $filtered_name" 
        # remove old files
        println("Removing $filtered_name")
        rm(filtered_name,force=true)
        # split command into pieces for the actual command
        split_command = split(command)
        cm = `$split_command`
        println(cm)
        read(cm)
    end
    # Now convert to spatialite
    dbase_name = joinpath(rawdir,"Cliffs.sql")
    rm(dbase_name,force=true)
    read(`ogr2ogr -overwrite -f 'SQLite' -dsco SPATIALITE=yes $dbase_name $filtered_name`)
    # Remove cliffs that are too close together
    cull_cliffs(dbase_name,0.0004,boundaries)
    # Remove problem cliffs, wish there was a more universal way...
    drop_cliffs(dbase_name,bad_cliff_ids)
    # Use the remaining cliffs to make gisfiles
    command = `ogr2ogr -f GPKG $final_shapefile $dbase_name final_list`
    println(command)
    read(command)
    return final_shapefile
end

#== 

Vegetation tools

==#

#==

Creating vegetation cover. Australia provides detailed vegetation cover information in
the NVIS geodatabase.  However, this simply uses an index number that must be joined with
a separate table in order to find out the vegetation type of an area ("feature"). 

Furthermore, the data are rasterised in 100mx100m blocks, which must be smoothed out
to look decent.

Steps:
1. Extract the desired spatial extent into an sqlite database
2. Add the key database as a new table
3. Create a new table by merging on nvisdsc1 = nvis_id retaining the 'shape' column
4. Add a line to geometry_columns advising of the new table
5. Convert to shapefile extracting layer named after new table
6. Dissolve neighbouring shapes together
7. Simplify to remove raster steps
==#

#== Timings

all QLD gdb -> spatialite: 2 minutes
all QLD spatialite -> merge: 48 minutes
all QLD choose area -> 5s

all QLD gdb -> spatialite: 2 minutes
all QLD spatialite choose area: 4.6s
all QLD spatialite merge: 1.1s
==#
 
make_local_data(d::NVISData,boundaries,rawdir) = begin
    n,s,w,e = boundaries
    fullname = get_filename(d)
    sname,sql_scratch = mktemp()
    vegecover_layer = d.layer
    vegecover_key = d.key
    vegecover_key_filename = d.key_filename
    fname = split(fullname)[end]
    if fname[(end-2):end] != "sql"
        master_db =  joinpath(rawdir,fname*".sql")
        if !isfile(master_db)
            println("No sign of $master_db, creating")
            read(`ogr2ogr -f SQLite -dsco SPATIALITE=YES -nlt PROMOTE_TO_MULTI $master_db $fullname`) #create SQLite db
            # create the local information, add key file
            read(`ogr2ogr -f SQLite -update $master_db $vegecover_key_filename`) #add key
            read(`spatialite $master_db "create table my_extent (id varchar,n int, s int, w int, e int);"`)
            read(`spatialite $master_db "insert into my_extent values ('current',0,0,0,0);"`)
        end
    else
        master_db = fullname
    end
    # check for local information
    bounds = read(`spatialite -silent $master_db "select n,s,w,e from my_extent where id = 'current'"`,String)
    ln,ls,lw,le = parse.(Int,split(bounds,"|"))
    if ln!=n || ls !=s || lw != w || le !=e
        println("Replacing old $bounds with new $boundaries")
        try
            read(`spatialite $master_db "drop table local_table"`)
            read(`spatialite $master_db "drop table merge_table"`)
        catch
            println("Error dropping tables; continuing")
        end
        
        read(`spatialite $master_db "create table local_table as select * from $vegecover_layer where mbrintersects(shape,ST_PolyFromText('polygon (($w $s, $e $s, $e $n, $w $n, $w $s ))'))"`)
        # merge with the key table
        read(`spatialite $master_db "create table merge_table as select shape,mvg_name,mvg_number,dominant_stratum from local_table join $vegecover_key where nvisdsc1 like $vegecover_key.nvis_id"`)
        read(`spatialite $master_db "select recovergeometrycolumn('merge_table','shape',4283,'MULTIPOLYGON')"`)
        read(`spatialite $master_db "create index mindex on merge_table (mvg_number)"`)
        read(`spatialite $master_db "update my_extent set n=$n, s=$s, w=$w, e=$e where id = 'current'"`)
        println("Finished updating master database $master_db for area $boundaries")
    else
        println("$master_db has correct extents, doing nothing")
    end
    return NVISData(master_db,vegecover_layer,vegecover_key,"")
end

extract_landcover(ltype::NVIS,limits,rawdir,local_data::NVISData,final_shapefile;linear=false,smooth=false) = begin
    target_shapefile = joinpath(rawdir,"vege_layer.gpkg")
    master_db = get_filename(local_data)
    #sname,sql_scratch = mktemp()
    # remove any old table
    try
        #read(`grass --exec db.execute input=$sname database=$master_db`)
        read(`spatialite $master_db "drop table vege_table"`)
    catch ErrorException
        println("Failed to drop table, continuing...")
    end
    # join them together
    read(`spatialite $master_db "create table vege_table as select shape,mvg_name,mvg_number from merge_table where $(ltype.phrase)" `)
    read(`spatialite $master_db "select RecoverGeometryColumn('vege_table','shape',4283,'MULTIPOLYGON')"`)
    #read(`grass --exec db.execute input=$sname database=$master_db`)
    #read(`ogr2ogr -update -sql @$sname -f SQLite $master_db $master_db`)
    # output as GISfile
    read(`ogr2ogr -overwrite -nln vege_layer -f GPKG $target_shapefile $master_db vege_table`)

    if !has_features(target_shapefile)
        println("No features in $target_shapefile, not generating $final_shapefile")
        return nothing
    end
    # Remove target
    rm(final_shapefile,force=true,recursive=true)
    sname,gscript = mktemp()
    write(gscript,"#!/bin/bash\n")
    write(gscript,"g.proj -c proj4=\"+proj=longlat +datum=WGS84 +no_defs\"\n")
    # Avoid weird leftover stuff after errors
    write(gscript,"g.remove -f type=vector pattern=\"temp_map_?\"\n")
    write(gscript,"v.in.ogr --overwrite -o snap=-1.0 input=$target_shapefile layer=vege_layer output=temp_map_0\n")
    n,s,w,e = limits
    write(gscript,"g.region e=$e w=$w s=$s n=$n res=100\n")
    # Add a dummy column of ones
    write(gscript,"v.db.addcolumn map=temp_map_0 columns='istree integer'\n")
    write(gscript,"v.db.update map=temp_map_0 column=istree value=1\n")
    if smooth
        write(gscript,"v.dissolve input=temp_map_0 column=istree output=temp_map_1 --overwrite\n")
        write(gscript,"v.generalize input=temp_map_1 method=chaiken threshold=0.0004 look_ahead=7 reduction=50 -l output=temp_map_2 --overwrite\n")
    else
        write(gscript,"v.dissolve input=temp_map_0 column=istree output=temp_map_2 --overwrite\n")
    end
    write(gscript,"v.clean input=temp_map_2 output=temp_map_3 tool=rmarea threshold=5000.0 --overwrite\n")
    write(gscript,"v.out.ogr --overwrite input=temp_map_3 type=auto output=$final_shapefile format=GPKG output_layer=multipolygons")
    close(gscript)
    read(`grass --exec sh $sname`)
    println("Generated $final_shapefile")
    return final_shapefile
end

"""
The MVG raster file is much smaller (61M for all of Australia) and also has a 100mx100m grid.
However it is a true raster in Albers projection so requires some fiddling to produce nice
looking shapefiles.  Some info:
* QGIS will not succeed in clipping unless the project projection is the same as the raster.
So make sure the project matches the raster.

* Perform the reclassification with the raster as it is fast and easy.

* Then convert to vectors (r.to.vect seemed OK)

* Then smooth. "Snake" and "Hermite" seemed sort of OK but still left some angular chunks

Abandoned using this data for now.
 
"""

"""Given a list of land use specifications, extract and merge, returning the name
of the final layer. Datafiles holds a list of data files of various types, to be
matched to the appropriate recipes."""
extract_multi(redo,boundaries,rawdir,datafiles,final_result,recipes) = begin
    if !redo && (isfile(final_result) || isdir(final_result))
        println("Skipping as $final_result exists")
        return final_result
    end
    all_shapefiles = []
    linear = get_width(recipes) > 0
    if linear
        specs = [get_recipes(recipes)[1]]
    else
        specs = get_recipes(recipes)
    end
    for one_spec in specs
        source_data = filter(x->data_type(one_spec)==typeof(x),datafiles)
        println("For $one_spec, using $source_data")
        for (num,sd) in enumerate(source_data)
            target_name = joinpath(rawdir,get_outfile_name(one_spec)*"$num")
            local_data = make_local_data(sd,boundaries,rawdir)
            result = extract_landcover(one_spec,boundaries,rawdir,local_data,target_name,
                                       linear=linear,smooth=smoothing(recipes))
            if !isnothing(result)
                push!(all_shapefiles,result)
            end
        end
    end
    # now merge them together
    if length(all_shapefiles) > 1
        println("Merging $all_shapefiles to make $final_result")
        new_layer = merge_gisfiles(all_shapefiles,final_result,boundaries)
        if isnothing(new_layer)
            println("Warning: no data found..at all")
            return nothing
        end
    elseif length(all_shapefiles) == 1
        println("Not merging, only one shapefile $(all_shapefiles[1])")
        cp(all_shapefiles[1],final_result)
    end
end

# change multiline enclosed shapes into polygons
lines_to_areas(in_shapefile,out_shapefile) = begin
    scratch_map = "grass_map"
    scratch_map_fixed = "multipolygons"  #needed name for merging
    run(`grass --exec v.in.ogr --overwrite input=$in_shapefile output=$scratch_map`)
    run(`grass --exec v.type --overwrite input=$scratch_map output=$scratch_map_fixed`)
    # We use the -c here as we do not expect islands in a simple area
    run(`grass --exec v.out.ogr -c --overwrite input=$scratch_map_fixed type=area output=$out_shapefile format=GPKG`)
end

#TODO:allow merging of line data as well
"""The provided shapefiles should have a layer named multipolygons"""
merge_gisfiles(gisfiles,final_file,boundaries) = begin
    # filter out empty shapefiles
    real_data = filter(x->are_there_areas(x),gisfiles)
    if length(real_data) == 1
        cp(real_data[1],final_file)
        return "multipolygons"
    elseif length(real_data) == 0
        return nothing
    else
        start_file = real_data[1]
    end
    old_output = "temp_map_0"
    sname,gscript = mktemp()
    write(gscript,"#!/bin/bash\n")
    write(gscript,"g.proj -c proj4=\"+proj=longlat +datum=WGS84 +no_defs\"\n")
    write(gscript,"v.in.ogr --overwrite -o min_area=0.001 input=$start_file layer=multipolygons output=$old_output\n")
    for (ct,sf) in enumerate(real_data[2:end])
        output_file = "merge_map_$ct"
        to_be_merged = "temp_map_$ct"
        n,s,w,e = boundaries
        write(gscript,"g.region e=$e w=$w s=$s n=$n res=100\n")
        write(gscript,"v.in.ogr --overwrite -o min_area=0.001 snap=1e-13 input=$sf layer=multipolygons output=$to_be_merged\n")
        write(gscript,"v.overlay --overwrite ainput=$old_output atype=area binput=$to_be_merged btype=area operator=or output=$output_file\n")
        #now have a new output file
        old_output = output_file
    end
    # remove target gisfile
    rm(final_file,force=true,recursive=true)
    write(gscript,"v.out.ogr input=$old_output type=area output=$final_file format=GPKG output_layer=multipolygons")
    close(gscript)
    read(`grass --exec sh $sname`)
    return "multipolygons"   #will be the name of the layer
end

#== Return true if there are areas defined in the map ==#
are_there_areas(mapname) = begin
    topo_dat = read(`ogrinfo -so $mapname multipolygons`, String)
    numareas = match(r".*Feature Count: ([0-9]+)",topo_dat)[1]
    numareas != "0"
end

#== Subtract one feature from another ==#
# May have issues as there were obscure problems in tgconstruct when estuaries
# were subtracted from the default landmass.
cut_out(base_shape,base_layer,cutout_shape,cutout_layer,final_result) = begin
    sname,gscript = mktemp()
    write(gscript,"#!/bin/bash\n")
    write(gscript,"v.in.ogr --overwrite -o snap=-1.0 input=$base_shape layer=$base_layer output=temp_map_0\n")
    write(gscript,"v.in.ogr --overwrite -o snap=-1.0 input=$cutout_shape layer=$cutout_layer output=temp_map_1\n")
    write(gscript,"v.overlay --overwrite ainput=temp_map_0 atype=area binput=temp_map_1 operator=not output=temp_map_2\n")
    write(gscript,"v.out.ogr input=temp_map_2 type=area output=$final_result format=GPKG output_layer=multipolygons")
    close(gscript)
    read(`grass --exec sh $sname`)
end

#==


Terragear tools

==#

do_decode(decoder,incoming_shape,area_type,boundaries,output_dir,linewidth;cliffs=false) = begin
    texture_option = "--texture-lines"
    if cliffs texture_option = "--texture-cliffs" end
    println("Decoding $incoming_shape")
    read(`$decoder --line-width $linewidth $texture_option --area-type $area_type $output_dir $incoming_shape`) 
end

# get a list of land types
get_land_types(priorities_file) = begin
    r = readlines(priorities_file)
    r = map(x->strip(x),r)
    #println("$r")
    filter!(x->length(x)>0 && x[1]!='#',r)
    #println("$r")
    fields = map(x->split(x),r)
    #println("$fields")
    return [x[1] for x in fields]
end

# Check that some features are available. If the file itself is
# nonexistent, that means false.
has_features(filename) = begin
    if !ispath(filename) return false end
    layers = read(`ogrinfo $filename`,String)
    lnames = [p[1] for p in eachmatch(r"[0-9]: ([A-Za-z_]+) (\([A-Za-z ]+\))",layers)]
    for one_layer in lnames
        result = pipeline(`ogrinfo -so $filename $one_layer`,`grep "Feature Count"`)
        info = split(read(result,String))
        println("Features for $filename/$one_layer: $info")
        if info[end] != "0" return true
        end
    end
    return false
end

# Find which column contains the geometry
get_geom_col(filename,layer_name) = begin
    result = pipeline(`ogrinfo -so $filename $layer_name`,`grep "Geometry Column"`)
    info = split(read(result,String))
    return info[end]
end

# Find out the type of the data file
get_gis_file_type(filename) = begin
    result = pipeline(`ogrinfo -so $filename`,`grep "using driver"`)
    info = split(read(result,String))
    return info[3][2:end]
end

calculate_filenames(bb) = begin
    n,s,w,e = bb
    topdir = ""
    lsymb = "e"
    latsymb = "s"
    if w < 0 lsymb = "w" end
    topdir = lsymb * "$(Integer(abs(floor(w/10)*10)))"
    if s > 0 latsymb = "n" end
    topdir = topdir * latsymb * "$(Integer(abs(floor(s/10)*10)))"
    # now the subdirectory
    long_int::Int64 = abs(floor(w))
    lat_int::Int64 = abs(floor(s))
    subdir = lsymb * "$long_int"*latsymb*"$lat_int"
    if abs(lat_int) < 22 xspan = 0.125
    elseif abs(lat_int) < 62 xspan = 0.25
    else error("Please add xspans for latitudes > 62")
    end
    ydiv::Int64 = floor((s - lat_int)/0.125)
    xdiv::Int64 = floor((w - long_int)/xspan)
    # now the detailed filename
    index_name = (long_int+180)<<14 + (lat_int+90)<<6 + ydiv <<3 + xdiv
    return topdir,subdir,index_name
end

#==

Special cliff handling code

==#

#==
Cliffs can be too closely spaced for the height interpolation algorithm
to handle them. So the following bit of SQL wizardry removes all cliffs
who overlap their neighbours by more than 50% of an area around
them.
==#

"""
Remove dense cliffs (lines found in spatialite file `input_file`) by creating
a bufferzone of size `bufferzone` degrees around them. Treat only the
area `boundaries` (top,bottom,left,right order).
"""
cull_cliffs(input_file,bufferzone,boundaries;final_table="final_list") = begin
    # sname,gscript = mktemp()
    sql_script = dirname(input_file)*"_extract.sql"
    n,s,w,e = boundaries
    # Remove working tables
    for old_table in ["cliff_buffs","buff_list","intersectors", "full_area",
                      "part_area", "dodgy_table",final_table]
        gscript = open(sql_script,"w")
        write(gscript,"drop table $old_table;\n")
        close(gscript)
        try
            run(`ogr2ogr -skipfailures -update -sql @$sql_script -f SQLite $input_file $input_file`)
        catch ErrorException
            println("Dropping table $old_table failed; continuing anyway")
        end
    end
    # create working tables anew
    sql_commands = [
    """create table cliff_buffs as select osm_id,geometry,ST_buffer(geometry,$bufferzone) as buff from lines where
      mbrminx(geometry)>$w and mbrminy(geometry)>$s and mbrmaxx(geometry)<$e and mbrmaxy(geometry)<$n;""",
    """-- Count number of intersections with each cliff
    create table intersectors as
        select osm_id, count(one_osm) as num_intersects from cliff_buffs,
           (select osm_id as one_osm, buff as one_cliff from cliff_buffs)
            where st_intersects(buff,one_cliff) = true
            group by osm_id;""",
    """-- Now join back up with original list, multi-intersectors only
    create table buff_list as
    select il.osm_id,buff from cliff_buffs inner join 
       (select osm_id from intersectors where num_intersects > 1) as il
	 on il.osm_id = cliff_buffs.osm_id;""",
    """-- Now calculate area of complete overlapping area
    create table full_area as select local_osm, local_cliff,osm_id, cliff_area, ST_area(ST_union(buff)) as full_area from buff_list,
   (select osm_id as local_osm, buff as local_cliff, ST_area(buff) as cliff_area from buff_list)
   where ST_intersects(local_cliff,buff) group by local_osm;""",
   """-- Now calculate area of incomplete overlapping area
   create table part_area as select local_osm, osm_id, cliff_area, ST_area(ST_union(buff)) as other_area from buff_list,
   (select osm_id as local_osm, buff as local_cliff, ST_area(buff) as cliff_area from buff_list)
   where ST_intersects(local_cliff,buff) and local_osm <> osm_id group by local_osm;""",
   """-- And find a list of cliffs that are not overlapped
   create table dodgy_table as 
     select fa.local_osm as osm_id, fa.local_cliff as geometry, fa.cliff_area, fa.full_area, part_area.other_area, 
            (fa.full_area - part_area.other_area)/fa.cliff_area as ratio 
             from full_area as fa join part_area on fa.local_osm = part_area.local_osm
             where ratio < 0.6;""",
    """-- And now select only those that are not overlapped
    create table $final_table as
    select osm_id,geometry from cliff_buffs where osm_id not in (select osm_id from dodgy_table);""",
    """-- Make sure geometry is recognised
    select RecoverGeometryColumn("$final_table","geometry",4326,"LINESTRING")"""]
    for one_command in sql_commands
        gscript = open(sql_script,"w")
        write(gscript,one_command)
        close(gscript)
        read(`ogr2ogr -update -sql @$sql_script -f SQLite $input_file $input_file`)
    end
end

"""
Sometimes cliffs are improperly defined in OSM or interact badly with the local environment,
mostly roads (which alter node heights to keep the road not too sloped). The OSM ids provided
are removed from the list of cliffs
"""
drop_cliffs(dbase_name,cliff_ids;cliff_list = "final_list") = begin
    for one_id in cliff_ids
        read(`ogr2ogr -update -sql "delete from $cliff_list where osm_id = $one_id"  $dbase_name $dbase_name`)
    end
end

"""
Remove all entries from `db` for which the alum code is in `prune_codes`
"""
prune_alum(db,full_table,field_name,prune_codes) = begin
    for code in prune_codes
        read(`spatialite $db "delete from $full_table where $field_name = $code"`)
    end
    read(`spatialite $db "vacuum"`)
end

"""
The WA ALUM data groups 100s of 1000s of polygons into a single geometry,
meaning that selecting by area does not yield any advantage. This routine
tries to expand out those areas that we care about

Adjust `db` so that all items with `al_code` appear as separate entries in
the data base
"""
blow_up_alum(db,full_table,field_name,al_code) = begin
    try
        read(`spatialite $db "drop table ref_table"`)
    catch
    end
    read(`spatialite $db "create table ref_table as select objectid,casttogeometrycollection(shape) as gc, numgeometries(casttogeometrycollection(shape)) as n,lu_codev8n from $full_table where $field_name = $al_code;"`)
    get_nums = readlines(`spatialite $db "select objectid,n from ref_table"`)
    vals = split.(get_nums,'|')
    for (id,total) in vals
        final = parse(Int32,total)
        if final < 10 continue end
        println("Expanding $final geometries for $al_code")
        for i in 1:final
            read(`spatialite $db "insert into $full_table (shape,$field_name) select casttomultipolygon(geometryn(gc,$i)),$field_name from ref_table where isempty(geometryn(gc,$i)) <> -1"`)
            if i%100 == 0 print("$i...") end
        end
        read(`spatialite $db "delete from $full_table where objectid = $id"`)
    end
end
