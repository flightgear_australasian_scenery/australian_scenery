# A very naive script to download all ViewfinderPanorama heights
# To use:
# run julia, at the prompt type: include(<this file name>)
# then type: download_a_lot(<dest_dir>,generate_list())
#
# Some empty files will be downloaded but they are removed.
#
const vp_url = "http://viewfinderpanoramas.org/dem3/"

generate_list() = begin
    full_list = []
    for lat in 'C':'J'
        for long in 49:53
            push!(full_list,"S$lat$long.zip")
        end
    end
    return full_list
end

download_a_lot(dest_dir,file_list) = begin
    for f in file_list
        dest_file = joinpath(dest_dir,f)
        try
            download(vp_url*f,dest_file)
            println("Finished $f")
        catch
            println("Skipping $f, failed")
        end
        # remove error message files
        ftype = read(`file -b $dest_file`,String)
        words = split(ftype)
        if words[1] != "Zip"
            println("Removing $f, is $ftype")
            rm(dest_file)
        end
    end
end


