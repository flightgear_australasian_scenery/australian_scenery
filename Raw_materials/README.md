# Places to find raw images for textures

## Aerial photography

Aerial photography should be "straight down", or "ortho photos".

* https://openaerialmap.org

## Building facades

## General

* https://flickr.com - make sure that modifications and commercial use are 
  allowed. Directly contacting photographers might be the easiest way to get 
  the appropriate permissions if you've found a photo that doesn't meet these
  criteria.