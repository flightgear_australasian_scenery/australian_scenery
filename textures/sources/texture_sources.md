# Texture sources

## Landcovers, except cliffs

All landcover texture swatches are derived from NSW LPI images, as
delivered through the OSM 'Edit' tool and screenshotted before processing
in GIMP.

Access to this imagery is governed by the CC-BY-4.0 license
(see https://www.spatial.nsw.gov.au/products_and_services/web_services).

### Notes

 `Terrain/australia-scrub.png`                    
 `Terrain/australia-scrub-hires.png`              
 `Terrain/australia-scrub-overlay.png`            
 `Terrain/australian-grassland-green-1.png`       
 `Terrain/sandstone-hires.png`                    
 `Terrain/australia-drypasture-winter-1.png`      
 `Terrain/australia-drypasture-winter-2.png`      
 `Terrain/australian-mine.png`                    
 `Terrain/australia_commercial.png`               
 `Terrain/australia_suburbs.png`                  
 `Terrain/australia-greenspace.png`               
 `Terrain/australia_suburbs_tree_mask.png`        
 `Terrain/australia-greenspace-mask.png`          
 `Terrain/australia-mangroves.png`                
 `Terrain/australian-grassland-summer-1-large.png`
 `Terrain/australian-grassland-summer-hires.png`  
 `Terrain/australian-grassland-summer-2-large.png`
 `Terrain/australia-drycrop-1.png`                
 `Terrain/australia-drycrop-2.png`                
 `Terrain/australia_country_golf.png`             
 `Terrain/australia_dirt_golf.png`                
 `Terrain/australia_agroforest_hardwood.png`      
 `Terrain/cemetery.png`                           
 `Terrain/australia_country_golf_mask.png`        
 `Terrain/australia_dirt_golf_mask.png`           
 `Terrain/cemetery_mask.png`                      

## Cliff texture

 `Terrain/australia_cliffs.png`                   
 `Terrain/australia_cliffs_hires.png`             

## Trees

 `Trees/australia-trees.png`                      
