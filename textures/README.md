# Textures

This directory contains texture squares for use in regional material
definitions.  The directory structure matches that found in fgdata
in the 'Textures' directory.

## Creating textures from photographs

See [the FlightGear wiki article](https://http://wiki.flightgear.org/Howto:Create_textures_from_photos).

## Use Git LFS for textures

Textures typically take up over 1MB of space each. Tracking these like normal
text files in Git would mean the repository gets very large as every version of
every texture file is stored. Please use Git
LFS to add these large files, that is, make sure you have 
[Git LFS](https://github.com/git-lfs/git-lfs) installed if you use Git locally.
Gitlab instructions on using Git LFS are 
[here](https://docs.gitlab.com/ee/workflow/lfs/manage_large_binaries_with_git_lfs.html)

This also means it is a good idea to give any textures that are meant to replace
old textures a temporary new name until they are accepted by the project, as the old work 
will otherwise be overwritten on the repository (but not on private copies until they
update).

## Sources

Please describe the source of each texture in an appropriate file in the
`sources` directory. Unfortunately this is necessary as, without appropriate 
copyright permission, textures cannot be distributed in FlightGear.

It is also useful for others to return to the source material
and attempt to improve or extend the texture.
