# Australian Scenery for FlightGear

This is where you can find tools to make scenery for Australia for use in the 
FlightGear flight simulator.

This project does not distribute the finished scenery. Rather,
it provides recipes for building the topography, and textures and objects for painting
and decorating the topography to look something like Australia.

## Using this project to improve your Australian scenery

Scenery in FlightGear is built out of several components: landcovers that
are draped over topography; textures and effects that are assigned
to landcovers; and objects that sit on (or in!) this scenery.
The landcover-draped topography is created before
FlightGear is run, and requires processing data from 
several sources; recipes for doing this are in the Recipes directory.
Read the instructions there for further information.

Once the topography has been created and written to an appropriate
directory, FlightGear will be able to load it and will render it according to the
specifications in the $$FG_ROOT/Materials directory.  Regional
definitions in this directory describe which Effects and Textures
to use. Effects and Textures are also contained in subdirectories of 
$$FG_ROOT.

This project therefore provides Australian regional definition files, together
with regional Textures. 

### Adding these regional definitions to your installation of FlightGear

The repository can be downloaded by clicking on the download icon 
(a cloud with a downward arrow) on the top-level page for this
repository.  If you are comfortable with git then cloning
is also an option, of course. 

After downloading and uncompressing (or cloning):
1. Rename "materials.xml" in $FG_ROOT/Materials/regions to a backup name,
   for example "materials-distrib.xml"
2. Copy the file "materials.xml" from this repository's regions directory
   to the $FG_ROOT/Materials/regions directory
3. Add all *.xml files found in our regions
   directory to $FG_ROOT/Materials/regions directory
4. Likewise, add all textures found in the Textures subdirectories to the
   corresponding subdirectories of $FG_ROOT/Textures

Linux/Mac users may wish to create symbolic links to files in the
local version of this repository instead of copying files. This will
save copying each time you update the repository.

Note that it is not necessary to create custom scenery in order to
use the regional textures. For further information see the
[FlightGear wiki](http://wiki.flightgear.org/Howto:Regional_texturing).

If using Git, you may need to do a `git lfs checkout` in order for the
texture files to be downloaded.

## Contributing

All contributions are welcomed. Here are some ways to help:

### In the virtual world

* Try out the scenery in your favourite place, let us know what could be improved
  by [raising an issue](https://gitlab.com/flightgear_australasian_scenery/australian_scenery/issues)
* Take some screenshots of generated scenery you like and post it to an appropriate
  FlightGear forum (e.g. [the "Media" subsection](https://forum.flightgear.org/viewforum.php?f=19)) 
* Post flights through scenery to YouTube and link to it in 
  [the FlightGear media forum](https://forum.flightgear.org/viewforum.php?f=19) 

### In the real world

* Take some photos (or get a friend to do so) that can be used to create textures
    * Isolated trees, building facades, aerial straight-down shots of landcovers
    * Have a drone with a good camera and live in Australia?  You can help! 

### On the computer

* Check the list of issues for jobs to do 
* Find photos on the web that can be used (check the copyright!) and link to them in
  the "photographs" directory
* Create some textures from your own photos or the ones here
* Create regional definitions using FlightGear stock textures and/or the ones here
* Become a maintainer and keep the project going
* Pick a favourite location and make it shine!
* Add recipes for scenery generation
* Work on the FlightGear wiki to improve documentation for scenery creators
* Program cool new stuff into terragear that improves terrain, e.g. waterfalls
* Create unique Australian scenery objects using Blender or other 3D modelling tool
  (Sydney Harbour Bridge anybody?)
* Create generic Australian objects (e.g. farmhouses that are not American barns)

### Actually making the contribution

Create a sign-in to Gitlab, if you don't have one already, and then
click on the "Request Access" link next to the project name.  Once
this is granted you can edit the repository directly.  If you're not
keen to break cover, then send a PM to frtps on the FlightGear forums
with a link to your contribution.

## Instructions for creating regional definitions

Many useful pages are collected in the FlightGear wiki 
[scenery enhancement category](http://wiki.flightgear.org/Category:Scenery_enhancement).

Note particularly the 
[description of how to do regional texturing](http://wiki.flightgear.org/Howto:Regional_texturing)
and the [procedural texturing page](http://wiki.flightgear.org/Procedural_Texturing) 
which describes how the various texture images are combined, and the appropriate numbers to 
assign to textures in regional definitions.

Some guidelines on photographs that can be used as texture source materials:
 - aerial photographs should be "straight down" or "ortho photos", taken in the middle
   of the day or in overcast conditions to avoid shadows
 - aerial photographs should ideally encompass around a kilometre horizontally and vertically
 - photographs of building facades should also be taken on overcast days and
   looking directly at the facade.
 - all photographs should be compatible with the GPL, i.e. both distribution and 
   modification should be allowed

## Some general principles

- The FlightGear project is the best place for this work to end up, so:
    - Texture image references in regional definitions should assume the FlightGear directory structure, e.g. "Terrain/cliffs.png"
    - Name textures with an "australia" prefix to avoid naming collisions with other FlightGear textures
    - Instructions on scenery creation are best placed in the FlightGear wiki unless it is specific to the organisation of this project
    - ...but a collection of useful links to the FlightGear wiki would help 
    - All images must have sufficient copyright permissions to allow distribution with FlightGear
- Unique scenery objects can be contributed directly to the FlightGear portal unless you would like others to improve your work first
